package com.leschenkoartem.app3test.ui.home.paging

import androidx.paging.DataSource
import com.leschenkoartem.domain.data.Article

class HomePagingInitFactory :
    DataSource.Factory<Int, Article>() {
    override fun create(): DataSource<Int, Article> = HomePagingInitDataSource()
}
