package com.leschenkoartem.app3test.ui.home

import com.leschenkoartem.domain.data.Article

interface OnItemClickListener {
    fun onItemClick(article:Article)
}