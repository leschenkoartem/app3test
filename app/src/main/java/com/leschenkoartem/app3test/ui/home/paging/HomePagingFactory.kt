package com.leschenkoartem.app3test.ui.home.paging

import androidx.paging.DataSource
import com.leschenkoartem.domain.data.Article
import com.leschenkoartem.domain.usecases.articles.GetArticlesListCase
import com.leschenkoartem.domain.usecases.articles.GetArticlesListLocalCase

class HomePagingFactory(
    private val category: Int,
    private val getArticlesListCase: GetArticlesListCase,
    private val articlesListLocalCase: GetArticlesListLocalCase
) :
    DataSource.Factory<Int, Article>() {
    override fun create(): DataSource<Int, Article> = HomePagingDataSource(
        category,
        getArticlesListCase,
        articlesListLocalCase
    )
}