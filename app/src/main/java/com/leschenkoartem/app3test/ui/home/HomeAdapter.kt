package com.leschenkoartem.app3test.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.leschenkoartem.app3test.R
import com.leschenkoartem.domain.data.Article
import kotlinx.android.synthetic.main.item_article_recycler.view.*
import kotlinx.android.synthetic.main.item_recycler_fist_tab.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.*

class HomeAdapter(
    private val listener: OnItemClickListener,
    private val listenerFirstItem: OnFirstItemClickListener
) :
    PagedListAdapter<Article, RecyclerView.ViewHolder>(ArticleAdapterDiff()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            -1,-2,-3,-4 -> FirstTabItemViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_fist_tab, parent, false)
            )
            else -> ArticleViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_article_recycler, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            -1 -> (holder as FirstTabItemViewHolder).bind(1, listenerFirstItem)
            -2 -> (holder as FirstTabItemViewHolder).bind(2, listenerFirstItem)
            -3 -> (holder as FirstTabItemViewHolder).bind(3, listenerFirstItem)
            -4 -> (holder as FirstTabItemViewHolder).bind(0, listenerFirstItem)
            else -> (holder as ArticleViewHolder).bind(getItem(position), listener)
        }

    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position)?.id?.toInt() ?: 0
    }

    class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            item: Article?,
            listener: OnItemClickListener
        ) {
            item?.let { article ->
                itemView.onClick {
                    listener.onItemClick(article)
                }
                itemView.tvArticleTitle.text = article.title

                itemView.tvDate.text = SimpleDateFormat("dd.MM.yy").format(article.dateCreated)

                article.picture.let { picture ->
                    Glide.with(itemView.ivArticle)
                        .load(picture)
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .into(itemView.ivArticle)

                }

            }
        }

    }

    class FirstTabItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(category: Int, listener: OnFirstItemClickListener) {
            itemView.btnCategory1.onClick {
                listener.onItemClick(1)
            }
            itemView.btnCategory2.onClick {
                listener.onItemClick(2)
            }
            itemView.btnCategory3.onClick {
                listener.onItemClick(3)
            }
            when (category) {
                1 -> itemView.btnCategory1.isSelected = true
                2 -> itemView.btnCategory2.isSelected = true
                3 -> itemView.btnCategory3.isSelected = true
            }
        }
    }


    class ArticleAdapterDiff : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean =
            oldItem.id == newItem.id
    }


}