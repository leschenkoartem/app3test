package com.leschenkoartem.app3test.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.leschenkoartem.app3test.ui.home.paging.HomePagingFactory
import com.leschenkoartem.app3test.ui.home.paging.HomePagingInitFactory
import com.leschenkoartem.domain.data.Article
import com.leschenkoartem.domain.usecases.articles.GetArticlesListCase
import com.leschenkoartem.domain.usecases.articles.GetArticlesListLocalCase

class HomeViewModel(
    private val getArticlesListCase: GetArticlesListCase,
    private val getArticlesListLocalCase: GetArticlesListLocalCase
) : ViewModel() {
    private var articlesPagedLiveData: LiveData<PagedList<Article>>? = null
    val articlesLiveData = MutableLiveData<PagedList<Article>>()
    private val observerPagedLiveData =
        Observer<PagedList<Article>> { articlesLiveData.postValue(it) }

    val selectedCategory = MutableLiveData<Int>()

    init {
        articlesLiveData.postValue(null)
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(1)
            .setPageSize(1)
            .build()

        articlesPagedLiveData = LivePagedListBuilder(HomePagingInitFactory(), config).build()
        articlesPagedLiveData?.observeForever(observerPagedLiveData)
    }

    fun loadCategory(category: Int) {
        selectedCategory.postValue(category)
        articlesLiveData.postValue(null)
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(10)
            .setPageSize(10)
            .build()

        articlesPagedLiveData = LivePagedListBuilder(
            HomePagingFactory(
                category,
                getArticlesListCase,
                getArticlesListLocalCase
            ), config
        ).build()
        articlesPagedLiveData?.observeForever(observerPagedLiveData)

    }

    override fun onCleared() {
        getArticlesListCase.dispose()
        getArticlesListLocalCase.dispose()
        articlesPagedLiveData?.removeObserver(observerPagedLiveData)
        super.onCleared()
    }

}