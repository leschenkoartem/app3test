package com.leschenkoartem.app3test.ui.article_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.leschenkoartem.domain.repositories.ArticlesRepository
import io.reactivex.disposables.Disposable

class ArticleDetailsViewModel(
    articleId: Long,
    articlesRepository: ArticlesRepository
) : ViewModel(){
    var dispose:Disposable
    val titleLiveData = MutableLiveData<String>()
    val imageLiveData = MutableLiveData<String>()
    val contentLiveData = MutableLiveData<String>()

    init {
        articlesRepository.getArticleById(articleId)
            .subscribe({
                titleLiveData.postValue(it.title)
                imageLiveData.postValue(it.picture)
                contentLiveData.postValue((it.content))
            },{}).also {
                dispose = it
            }
    }

    override fun onCleared() {
        dispose.dispose()
        super.onCleared()
    }
}