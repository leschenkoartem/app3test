package com.leschenkoartem.app3test.ui.home.paging

import androidx.paging.PageKeyedDataSource
import com.leschenkoartem.domain.data.Article

class HomePagingInitDataSource: PageKeyedDataSource<Int, Article>() {
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Article>
    ) {
        callback.onResult(listOf(Article(-4)), null, 0)

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
    }
}