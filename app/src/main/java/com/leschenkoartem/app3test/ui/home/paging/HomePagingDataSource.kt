package com.leschenkoartem.app3test.ui.home.paging

import androidx.paging.PageKeyedDataSource
import com.leschenkoartem.domain.data.Article
import com.leschenkoartem.domain.usecases._base.observer.SimpleDisposableSingleObserver
import com.leschenkoartem.domain.usecases.articles.GetArticlesListCase
import com.leschenkoartem.domain.usecases.articles.GetArticlesListLocalCase

class HomePagingDataSource(
    private val category: Int,
    private val articlesListCase: GetArticlesListCase,
    private val articlesListLocalCase: GetArticlesListLocalCase
) :
    PageKeyedDataSource<Int, Article>() {
    private var pageCounter = 0

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Article>
    ) {
        articlesListCase.execute(object : SimpleDisposableSingleObserver<List<Article>>() {
            override fun onSuccess(t: List<Article>) {
                super.onSuccess(t)
                callback.onResult(listOf(Article((category*-1).toLong()))+t, null, pageCounter++)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                loadInitialLocal(callback,GetArticlesListCase.Param(category, pageCounter, params.requestedLoadSize))
            }
        }, GetArticlesListCase.Param(category, pageCounter, params.requestedLoadSize))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        articlesListCase.execute(object : SimpleDisposableSingleObserver<List<Article>>() {
            override fun onSuccess(t: List<Article>) {
                super.onSuccess(t)
                callback.onResult(t,  pageCounter++)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                loadAfterLocal(callback,GetArticlesListCase.Param(category, pageCounter, params.requestedLoadSize))
            }
        }, GetArticlesListCase.Param(category, pageCounter, params.requestedLoadSize))
    }

    private fun loadInitialLocal(callback: LoadInitialCallback<Int, Article>, params: GetArticlesListCase.Param) {
        articlesListLocalCase.execute(object : SimpleDisposableSingleObserver<List<Article>>() {
            override fun onSuccess(t: List<Article>) {
                super.onSuccess(t)
                callback.onResult(listOf(Article((category*-1).toLong()))+t, null, pageCounter++)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
            }
        },params = params)
    }

    private fun loadAfterLocal(callback: LoadCallback<Int, Article>, params: GetArticlesListCase.Param) {
        articlesListLocalCase.execute(object : SimpleDisposableSingleObserver<List<Article>>() {
            override fun onSuccess(t: List<Article>) {
                super.onSuccess(t)
                callback.onResult(t,  pageCounter++)
            }
        },params = params)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        TODO("Not yet implemented")
    }
}