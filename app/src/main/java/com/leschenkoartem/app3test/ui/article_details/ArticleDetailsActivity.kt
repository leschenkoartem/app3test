package com.leschenkoartem.app3test.ui.article_details

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Html.fromHtml
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.leschenkoartem.app3test.R
import kotlinx.android.synthetic.main.activity_article_details.*

import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

const val ARG_ARTICLE_ID = "ARG_ARTICLE_ID"

class ArticleDetailsActivity : AppCompatActivity() {

    private val viewModel: ArticleDetailsViewModel by viewModel {
        parametersOf(intent.getLongExtra(ARG_ARTICLE_ID, -1))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_details)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.titleLiveData.observe(this, Observer { tvTitle.text = it })
        viewModel.contentLiveData.observe(this, Observer {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvContent.text = fromHtml(it, Html.FROM_HTML_MODE_COMPACT)
            } else {
                tvContent.text = fromHtml(it)
            }
        })
        viewModel.imageLiveData.observe(this, Observer {
            Glide.with(this)
                .load(it)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(ivArticle)

        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
