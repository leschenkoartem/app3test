package com.leschenkoartem.app3test.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.leschenkoartem.app3test.R
import com.leschenkoartem.app3test.ui.article_details.ARG_ARTICLE_ID
import com.leschenkoartem.app3test.ui.article_details.ArticleDetailsActivity
import com.leschenkoartem.domain.data.Article
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.support.v4.startActivity
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by sharedViewModel()

    private val adapter = HomeAdapter(
        object : OnItemClickListener {
            override fun onItemClick(article: Article) {
                startActivity<ArticleDetailsActivity>(ARG_ARTICLE_ID to article.id)
            }
        },
        object : OnFirstItemClickListener {
            override fun onItemClick(category: Int) {
                homeViewModel.loadCategory(category)
            }
        }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recycler.layoutManager = LinearLayoutManager(activity)
        recycler.adapter = adapter

        homeViewModel.articlesLiveData.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
        // homeViewModel.loadCategory(1)
    }
}
