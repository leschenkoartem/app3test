package com.leschenkoartem.app3test

import android.app.Application
import com.leschenkoartem.data.local.db.databaseModule
import com.leschenkoartem.data.remoteModule
import com.leschenkoartem.domain.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App3App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@App3App)
            modules(
                listOf(
                    presentationModule,
                    remoteModule,
                    domainModule,
                    databaseModule
                )
            )
        }
    }
}