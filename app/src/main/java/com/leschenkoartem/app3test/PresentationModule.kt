package com.leschenkoartem.app3test


import com.leschenkoartem.app3test.ui.article_details.ArticleDetailsViewModel
import com.leschenkoartem.app3test.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val presentationModule = module {
    viewModel { HomeViewModel(get(), get()) }
    viewModel { (articleId: Long) ->
        ArticleDetailsViewModel(
            articleId = articleId,
            articlesRepository = get()
        )
    }
}