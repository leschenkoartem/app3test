package com.leschenkoartem.data.mappers

import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class ArticleRemoteToDomainMappersKtTest {

    @Test
    fun toDomain() {
        val f = SimpleDateFormat("dd.MM.yy")
        val d = Date(1589799600000)
        val d2 = Date()
        val s = f.format(d)
        val s2 = f.format(d2)
        println("D - ${d.time}")
        println("D2 - ${d2.time}")
        println(s)
        println(s2)
        val date = Date("1589799600".toLong())
        assert(date.time == 1589799600L)


    }
}