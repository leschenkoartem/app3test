package com.leschenkoartem.data.repository

import com.leschenkoartem.data.datasources.ArticlesLocalDataSource
import com.leschenkoartem.data.datasources.ArticlesRemoteDataSource
import com.leschenkoartem.data.mappers.toDomain
import com.leschenkoartem.data.memory.ArticlesMemoryDataSourceImpl
import com.leschenkoartem.domain.data.Article
import com.leschenkoartem.domain.repositories.ArticlesRepository
import io.reactivex.Single

class ArticlesRepositoryImpl(
    private val articlesRemoteDataSource: ArticlesRemoteDataSource,
    private val articlesMemoryDataSource: ArticlesMemoryDataSourceImpl,
    private val articlesLocalDataSource: ArticlesLocalDataSource
) : ArticlesRepository {
    override fun getArticlesList(
        category: Int,
        page: Int,
        requestedLoadSize: Int
    ): Single<List<Article>> =
        articlesRemoteDataSource.getListArticles(
            category = category,
            page = page,
            loadSize = requestedLoadSize
        )
            .map { it.toDomain() }
            .flatMapSingle { listArticles ->
                articlesMemoryDataSource.setListArticles(listArticles)
                articlesLocalDataSource.setListArticles(listArticles, category)
                Single.create<List<Article>> {
                    it.onSuccess(listArticles)
                }
            }


    override fun getArticleById(id: Long): Single<Article> =
        articlesMemoryDataSource.getArticleById(id).toSingle()

    override fun getArticlesListLocal(
        category: Int,
        page: Int,
        requestedLoadSize: Int
    ): Single<List<Article>> =
        articlesLocalDataSource.getPageListArticles(category, page, requestedLoadSize)
            .flatMapSingle { articleList ->
                articlesMemoryDataSource.setListArticles(articleList)
                Single.create<List<Article>> {
                    it.onSuccess(articleList)
                }
            }

}