package com.leschenkoartem.data

import com.leschenkoartem.data.datasources.ArticlesLocalDataSource
import com.leschenkoartem.data.datasources.ArticlesRemoteDataSource
import com.leschenkoartem.data.local.datasources.ArticlesLocalDataSourceImp
import com.leschenkoartem.data.memory.ArticlesMemoryDataSourceImpl
import com.leschenkoartem.data.remote.api.ArticlesApi
import com.leschenkoartem.data.remote.createOkHttpClient
import com.leschenkoartem.data.remote.createRetrofit
import com.leschenkoartem.data.remote.createWebService
import com.leschenkoartem.data.remote.datasources.ArticlesRemoteDataSourceImpl
import com.leschenkoartem.data.repository.ArticlesRepositoryImpl
import com.leschenkoartem.domain.repositories.ArticlesRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val OKHTTP = "OKHTTP"
val RETROFIT = "RETROFIT"
val URL_STUB = "https://www.heikoschrang.de"
val LOCAL = "LOCAL"
val REMOTE = "REMOTE"

val remoteModule = module {

    single(named(OKHTTP)) {
        createOkHttpClient()
    }
    single(named(RETROFIT)) {
        createRetrofit(
            okHttpClient = get(named(OKHTTP)),
            url = URL_STUB
        )
    }

    single { createWebService<ArticlesApi>(get(named(RETROFIT))) }


    single<ArticlesRemoteDataSource> { ArticlesRemoteDataSourceImpl(get()) }
    single<ArticlesLocalDataSource> { ArticlesLocalDataSourceImp(get()) }
    single { ArticlesMemoryDataSourceImpl() }
    single<ArticlesRepository> {
        ArticlesRepositoryImpl(get(), get(), get())
    }

}