package com.leschenkoartem.data.local.db

import android.content.Context
import androidx.room.Room

const val DATABASE_NAME = "App3DataBase.db"

fun createGpDatabase(applicationContext: Context) = Room.databaseBuilder(
    applicationContext,
    App3DataBase::class.java,
    DATABASE_NAME
)
    .fallbackToDestructiveMigration()
    .allowMainThreadQueries() // Temporary threading workaround.
    .build()