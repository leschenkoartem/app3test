package com.leschenkoartem.data.local.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "article")
data class ArticleEntity(
    @PrimaryKey var id: Long,
    val videoDuration: String = "",
    val title: String = "",
    val content: String = "",
    val dateCreated: Date = Date(),
    val picture: String = "",
    val category: Int = 0
)