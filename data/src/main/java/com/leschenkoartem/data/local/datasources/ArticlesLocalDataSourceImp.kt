package com.leschenkoartem.data.local.datasources

import com.leschenkoartem.data.datasources.ArticlesLocalDataSource
import com.leschenkoartem.data.local.db.dao.ArticleDao
import com.leschenkoartem.data.mappers.toDomain
import com.leschenkoartem.data.mappers.toLocal
import com.leschenkoartem.domain.data.Article
import io.reactivex.Maybe

class ArticlesLocalDataSourceImp(
    private val articleDao: ArticleDao
): ArticlesLocalDataSource {
    override fun getPageListArticles(
        category: Int,
        page: Int,
        loadSize: Int
    ): Maybe<List<Article>> =
        articleDao.getPageListArticles(category,page,loadSize)
            .map { it.toDomain() }




    override fun setListArticles(
        list: List<Article>,
        category: Int
    ) {
        articleDao.insert(list.toLocal(category))
    }
}