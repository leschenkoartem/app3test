package com.leschenkoartem.data.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.leschenkoartem.data.local.db.model.ArticleEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

@Dao
interface ArticleDao {
    @Query("SELECT * FROM article  WHERE id = :id")
    fun getById(id: Long): Maybe<ArticleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(articles: List<ArticleEntity>)

    @Query("SELECT * FROM article")
    fun getAllArticles():List<ArticleEntity>

    @Query("SELECT * FROM article WHERE category = :category ORDER BY id DESC LIMIT :loadSize OFFSET :page * :loadSize  ")
    fun getPageListArticles(category: Int, page: Int, loadSize: Int):Maybe<List<ArticleEntity>>

}