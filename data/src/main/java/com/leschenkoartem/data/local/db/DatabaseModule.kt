package com.leschenkoartem.data.local.db

import org.koin.dsl.module
import org.koin.android.ext.koin.androidContext

val databaseModule = module {
    single { createGpDatabase(androidContext()) }

    single { (get() as App3DataBase).getArticleDao() }
}