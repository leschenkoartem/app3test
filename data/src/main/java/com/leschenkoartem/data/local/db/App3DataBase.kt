package com.leschenkoartem.data.local.db

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteOpenHelper
import com.leschenkoartem.data.local.db.converters.DateTypeConverter
import com.leschenkoartem.data.local.db.dao.ArticleDao
import com.leschenkoartem.data.local.db.model.ArticleEntity

@Database(entities = [ArticleEntity::class],version = 1)
@TypeConverters(DateTypeConverter::class)
abstract class App3DataBase: RoomDatabase() {
    abstract fun getArticleDao():ArticleDao

}