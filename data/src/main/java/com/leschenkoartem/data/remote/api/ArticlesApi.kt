package com.leschenkoartem.data.remote.api

import com.leschenkoartem.data.remote.data.articles_list.ArticlesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticlesApi {
    @GET("/applications/app/content/list")
    fun getItemsList(
        @Query("category") category: Int? = null,
        @Query("page") pageNum: Int? = null,
        @Query("per_page") perPage: Int? = null,
        @Query("order") order: String = "desc"

    ): Single<ArticlesResponse>
}