package com.leschenkoartem.data.remote.data.articles_list

import com.google.gson.annotations.SerializedName

data class Pages(@SerializedName("last")
                 val last: Int = 0,
                 @SerializedName("count")
                 val count: Int = 0,
                 @SerializedName("articles")
                 val articles: Int = 0,
                 @SerializedName("first")
                 val first: Int = 0)