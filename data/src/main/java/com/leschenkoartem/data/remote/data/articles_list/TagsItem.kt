package com.leschenkoartem.data.remote.data.articles_list

import com.google.gson.annotations.SerializedName

data class TagsItem(@SerializedName("tag")
                    val tag: String = "",
                    @SerializedName("slug")
                    val slug: String = "")