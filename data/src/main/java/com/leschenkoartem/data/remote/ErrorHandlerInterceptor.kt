package com.leschenkoartem.data.remote


import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.leschenkoartem.domain.exceptions.DataSourceException
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

const val MESSAGE = "message"

class ErrorHandlerInterceptor : Interceptor {


    override fun intercept(chain: Interceptor.Chain): Response {

        val response: Response
        try {
            response = chain.proceed(chain.request())
        } catch (e: IOException) {
            e.printStackTrace()
            throw e
        }

        if (!response.isSuccessful) {
            val body = response.body?.string()

            println("<-- code = ${response.code} body = $body")

            when (response.code) {
                400 -> {
                    //
                }
                409 -> {
                    //
                }
                498 -> {
                    // WARN: async operation
                }
                500 -> {
                    // Handle internal server error here
                }
            }

            try {
                when (val jsonElement = Gson().fromJson(body, JsonElement::class.java)) {
                    is JsonObject -> {
                        jsonElement.keySet()
                            .forEach { key ->
                                val errorMessage = (jsonElement.get(key).asString)
                                when (key) {
                                    MESSAGE -> throw DataSourceException(errorMessage)
                                }
                            }
                    }
                }
            } catch (e: Exception) {
                when (e) {
                    is DataSourceException -> {
                        throw e
                    }
                    else -> {
                        val errorMessage = "${response.code} - ${response.message}"
                        e.printStackTrace()
                        throw DataSourceException(errorMessage)
                    }
                }

            }


        }

        return response
    }
}