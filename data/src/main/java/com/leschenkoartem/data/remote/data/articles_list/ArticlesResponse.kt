package com.leschenkoartem.data.remote.data.articles_list

import com.google.gson.annotations.SerializedName

data class ArticlesResponse(@SerializedName("pages")
                            val pages: Pages,
                            @SerializedName("articles")
                            val articles: List<ArticlesItem>?)