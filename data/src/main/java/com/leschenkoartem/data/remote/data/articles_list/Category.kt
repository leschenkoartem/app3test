package com.leschenkoartem.data.remote.data.articles_list

import com.google.gson.annotations.SerializedName

data class Category(@SerializedName("id")
                    val id: String = "",
                    @SerializedName("title")
                    val title: String = "",
                    @SerializedName("picture")
                    val picture: String = "")