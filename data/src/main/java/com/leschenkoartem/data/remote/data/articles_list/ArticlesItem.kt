package com.leschenkoartem.data.remote.data.articles_list

import com.google.gson.annotations.SerializedName

data class ArticlesItem(@SerializedName("video_duration")
                        val videoDuration: String? = "",
                        @SerializedName("source")
                        val source: String? = null,
                        @SerializedName("picture_collection")
                        val pictureCollection: String = "",
                        @SerializedName("title")
                        val title: String = "",
                        @SerializedName("content")
                        val content: String = "",
                        @SerializedName("podcast_duration")
                        val podcastDuration: String = "",
                        @SerializedName("video_other")
                        val videoOther: String? = null,
                        @SerializedName("podcast")
                        val podcast: String? = null,
                        @SerializedName("id")
                        val id: String = "",
                        @SerializedName("slug")
                        val slug: String = "",
                        @SerializedName("picture_width")
                        val pictureWidth: String = "",
                        @SerializedName("hit_counter")
                        val hitCounter: String = "",
                        @SerializedName("date_created")
                        val dateCreated: String = "",
                        @SerializedName("podcast_mime")
                        val podcastMime: String? = null,
                        @SerializedName("date_custom")
                        val dateCustom: String = "",
                        @SerializedName("picture_height")
                        val pictureHeight: String = "",
                        @SerializedName("picture")
                        val picture: String = "",
                        @SerializedName("recommended")
                        val recommended: String = "",
                        @SerializedName("tags")
                        val tags: List<TagsItem>?,
                        @SerializedName("video_youtube")
                        val videoYoutube: String? = "",
                        @SerializedName("picture_mime")
                        val pictureMime: String = "",
                        @SerializedName("podcast_link")
                        val podcastLink: String? = null,
                        @SerializedName("video_bitchute")
                        val videoBitchute: String? = null,
                        @SerializedName("second_title")
                        val secondTitle: String? = null,
                        @SerializedName("category")
                        val category: Category,
                        @SerializedName("video_vimeo")
                        val videoVimeo: String? = null,
                        @SerializedName("hit_rating")
                        val hitRating: String = "",
                        @SerializedName("status")
                        val status: String = "",
                        @SerializedName("teaser")
                        val teaser: String = "")