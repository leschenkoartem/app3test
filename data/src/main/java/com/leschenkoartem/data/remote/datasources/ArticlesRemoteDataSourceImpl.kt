package com.leschenkoartem.data.remote.datasources

import com.leschenkoartem.data.datasources.ArticlesRemoteDataSource
import com.leschenkoartem.data.remote.api.ArticlesApi
import com.leschenkoartem.data.remote.data.articles_list.ArticlesItem
import io.reactivex.Maybe

class ArticlesRemoteDataSourceImpl(
    private val api: ArticlesApi
) : ArticlesRemoteDataSource {
    override fun getListArticles(category: Int, page: Int, loadSize: Int): Maybe<List<ArticlesItem>> =
        api.getItemsList(
            category = category,
            pageNum = page,
            perPage = loadSize)
            .flatMapMaybe {response ->
                Maybe.create<List<ArticlesItem>> {
                    if(response.articles != null){
                        it.onSuccess(response.articles)
                    }else{
                        it.onComplete()
                    }
                }
            }

}