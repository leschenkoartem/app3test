package com.leschenkoartem.data.datasources

import com.leschenkoartem.data.remote.data.articles_list.ArticlesItem
import com.leschenkoartem.domain.data.Article
import io.reactivex.Completable
import io.reactivex.Maybe

interface ArticlesRemoteDataSource {
    fun getListArticles(category: Int, page: Int, loadSize: Int): Maybe<List<ArticlesItem>>
}