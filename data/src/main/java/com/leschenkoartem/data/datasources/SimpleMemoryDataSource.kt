package com.leschenkoartem.data.datasources

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

abstract class SimpleMemoryDataSource<T> : ISimpleDataSource<T> {

    val cacheSubject = BehaviorSubject.create<T>()

    override fun getData() = observeData().firstElement()
    override fun observeData(): Observable<T> = cacheSubject
    override fun setData(data: T) = Completable.fromCallable { cacheSubject.onNext(data) }
}