package com.leschenkoartem.data.datasources

import com.leschenkoartem.domain.data.Article
import io.reactivex.Completable
import io.reactivex.Maybe

interface ArticlesMemoryDataSource {
    fun getArticleById(id: Long): Maybe<Article>

    fun setListArticles(articles: List<Article>): Completable
}