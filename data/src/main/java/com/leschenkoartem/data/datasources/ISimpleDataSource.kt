package com.leschenkoartem.data.datasources

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

interface ISimpleDataSource<T> {
    fun getData(): Maybe<T>
    fun observeData(): Observable<T>
    fun setData(data: T): Completable
}