package com.leschenkoartem.data.datasources

import com.leschenkoartem.domain.data.Article
import io.reactivex.Maybe

interface ArticlesLocalDataSource {
    fun getPageListArticles(category: Int, page: Int, loadSize: Int): Maybe<List<Article>>
    fun setListArticles(
        list: List<Article>,
        category: Int
    )
}