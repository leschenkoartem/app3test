package com.leschenkoartem.data.mappers

import com.leschenkoartem.data.local.db.model.ArticleEntity
import com.leschenkoartem.data.remote.data.articles_list.ArticlesItem
import com.leschenkoartem.domain.data.Article
import java.util.*


fun List<ArticlesItem>.toDomain(): List<Article> = map { it.toDomain() }

fun ArticlesItem.toDomain(): Article = Article(
    id = id.toLong(),
    content = content,
    dateCreated = dateCreated.toDomain(),
    picture = picture,
    title = title,
    videoDuration = videoDuration?:"",
    videoYoutube = videoYoutube?:""
)

private fun String.toDomain(): Date = Date(this.toLong()*1000) // second to millisecond
