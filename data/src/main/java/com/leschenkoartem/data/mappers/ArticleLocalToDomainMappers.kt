package com.leschenkoartem.data.mappers

import com.leschenkoartem.data.local.db.model.ArticleEntity
import com.leschenkoartem.data.remote.data.articles_list.ArticlesItem
import com.leschenkoartem.domain.data.Article
import java.util.*


fun List<ArticleEntity>.toDomain(): List<Article> = map { it.toDomain() }

fun ArticleEntity.toDomain(): Article = Article(
    id = id,
    content = content,
    dateCreated = dateCreated,
    picture = picture,
    title = title,
    videoDuration = videoDuration?:""
)