package com.leschenkoartem.data.mappers

import com.leschenkoartem.data.local.db.model.ArticleEntity
import com.leschenkoartem.domain.data.Article

fun List<Article>.toLocal(category: Int): List<ArticleEntity> = map { it.toLocal(category) }

fun Article.toLocal(category: Int): ArticleEntity = ArticleEntity(
    id = id,
    content = content,
    dateCreated = dateCreated,
    picture = picture,
    title = title,
    videoDuration = videoDuration?:"",
    category = category
)