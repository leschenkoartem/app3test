package com.leschenkoartem.data.memory

import com.leschenkoartem.data.datasources.ArticlesMemoryDataSource
import com.leschenkoartem.domain.data.Article
import io.reactivex.Completable
import io.reactivex.Maybe
import java.lang.Error

class ArticlesMemoryDataSourceImpl : ArticlesMemoryDataSource {
    private val cache = mutableMapOf<Long, Article>()
    override fun getArticleById(id: Long): Maybe<Article> {
        return Maybe.create { maybe ->
            if (cache.containsKey(id)){
                cache[id]?.let { maybe.onSuccess(it) }
            }else{
                maybe.onError(Error("No cashed data!!!"))
            }
        }
    }

    override fun setListArticles(articles: List<Article>): Completable {
        articles.map {
            cache.put(it.id, it)
        }
        return Completable.complete()
    }


}