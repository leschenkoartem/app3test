package com.leschenkoartem.domain.exceptions

open class DataSourceException(val errorMessage: String) : Exception() {

    override fun toString(): String {
        return errorMessage
    }
}