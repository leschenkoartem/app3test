package com.leschenkoartem.domain.usecases.articles

import com.leschenkoartem.domain.usecases._base.SingleUseCase
import com.leschenkoartem.domain.data.Article
import com.leschenkoartem.domain.repositories.ArticlesRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GetArticlesListCase(
    private val advertsRepository: ArticlesRepository
) : SingleUseCase<List<Article>, GetArticlesListCase.Param>() {
    override fun buildUseCaseSingle(params: Param?): Single<List<Article>> =
        advertsRepository.getArticlesList(params!!.category, params.page, params.requestedLoadSize)
            .subscribeOn(Schedulers.io())

    class Param(val category: Int, val page: Int, val requestedLoadSize: Int)
}