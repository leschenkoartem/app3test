package com.leschenkoartem.domain.usecases.articles

import com.leschenkoartem.domain.usecases._base.SingleUseCase
import com.leschenkoartem.domain.data.Article
import com.leschenkoartem.domain.repositories.ArticlesRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GetArticlesListLocalCase(
    private val advertsRepository: ArticlesRepository
) : SingleUseCase<List<Article>, GetArticlesListCase.Param>() {
    override fun buildUseCaseSingle(params: GetArticlesListCase.Param?): Single<List<Article>> =
        advertsRepository.getArticlesListLocal(params!!.category, params.page, params.requestedLoadSize)
            .subscribeOn(Schedulers.io())
}