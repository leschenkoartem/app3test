package com.leschenkoartem.domain.usecases._base.observer

import io.reactivex.observers.DisposableSingleObserver


abstract class SimpleDisposableSingleObserver<T> : DisposableSingleObserver<T>() {

    override fun onSuccess(t: T) {
        // Override to handle result
    }

    override fun onError(e: Throwable) {
        e.printStackTrace()
    }
}