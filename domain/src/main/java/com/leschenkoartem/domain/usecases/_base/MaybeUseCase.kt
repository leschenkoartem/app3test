package com.leschenkoartem.domain.usecases._base

import io.reactivex.Maybe
import io.reactivex.MaybeTransformer
import io.reactivex.observers.DisposableMaybeObserver


abstract class MaybeUseCase<Results, in Params> : BaseReactiveUseCase() {

    abstract fun buildUseCaseMaybe(params: Params? = null): Maybe<Results>

    fun execute(observer: DisposableMaybeObserver<Results>, params: Params? = null) = execute(observer, null, params)

    fun execute(observer: DisposableMaybeObserver<Results>, transformer: MaybeTransformer<Results, Results>? = null, params: Params? = null) {
        var maybe = buildUseCaseMaybe(params)
        if (transformer != null) {
            maybe = maybe.compose(transformer)
        }
        addDisposable(maybe.subscribeWith(observer))
    }
}