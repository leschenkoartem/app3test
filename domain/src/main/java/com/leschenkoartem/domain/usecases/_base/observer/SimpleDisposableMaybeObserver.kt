package com.leschenkoartem.domain.usecases._base.observer

import io.reactivex.observers.DisposableMaybeObserver


abstract class SimpleDisposableMaybeObserver<T> : DisposableMaybeObserver<T>() {

    override fun onComplete() {
        //
    }

    override fun onSuccess(t: T) {
        // Override to handle result
    }

    override fun onError(e: Throwable) {
        e.printStackTrace()
    }
}