package com.leschenkoartem.domain.usecases._base.observer

import com.leschenkoartem.domain.util.SingleLiveEvent
import io.reactivex.observers.DisposableCompletableObserver


abstract class SimpleDisposableCompletableObserver() : DisposableCompletableObserver() {

    var commonErrorEvent: SingleLiveEvent<String>? = null

    constructor(showErrorEvent: SingleLiveEvent<String>) : this() {
        this.commonErrorEvent = showErrorEvent
    }

    override fun onComplete() {
        // Override to handle result
    }

    override fun onError(e: Throwable) {
        println("Error: ${e.localizedMessage}")
        e.printStackTrace()

        when (e) {
            is IllegalStateException -> commonErrorEvent?.postValue(e.message)
            else -> commonErrorEvent?.postValue(e.toString())
        }
    }
}