package com.leschenkoartem.domain


import com.leschenkoartem.domain.usecases.articles.GetArticlesListCase
import com.leschenkoartem.domain.usecases.articles.GetArticlesListLocalCase
import org.koin.dsl.module


val domainModule = module {
    factory { GetArticlesListCase(get()) }
    factory { GetArticlesListLocalCase(get()) }

}