package com.leschenkoartem.domain.data

import java.util.*

data class Article(
    val id: Long,
    val videoDuration: String = "",
    val title: String = "",
    val content: String = "",
    val dateCreated: Date = Date(),
    val picture: String = "",
    val videoYoutube: String = ""
)