package com.leschenkoartem.domain.repositories

import com.leschenkoartem.domain.data.Article
import io.reactivex.Maybe
import io.reactivex.Single

interface ArticlesRepository {

    fun getArticlesList(category: Int, page: Int, requestedLoadSize: Int): Single<List<Article>>
    fun getArticlesListLocal(category: Int, page: Int, requestedLoadSize: Int): Single<List<Article>>
    fun getArticleById(id:Long): Single<Article>
}